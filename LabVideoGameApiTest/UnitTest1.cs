using LabVideoGameApi.Controllers;
using LabVideoGameApi.dto;
using LabVideoGameApi.model;
using LabVideoGameApi.service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;

namespace LabVideoGameApiTest
{
    [TestClass]
    public class UserControllerTests
    {
        [TestMethod]
        public void Register_ExistingUser_ReturnsConflict()
        {
            var existtUs = new UserModel { Username = "Herlan5", Password = "HerlanForever5", Role = "User" };
            var authServM = new Mock<IAuthenticServ>();
            authServM.Setup(service => service.Register(existtUs)).Throws(new InvalidOperationException("UserName Exist"));
            var control = new AuthenticController(authServM.Object);
            var result = control.Register(existtUs);
            var badRequestResult = Xunit.Assert.IsType<BadRequestObjectResult>(result);
            Xunit.Assert.Equal(400, badRequestResult.StatusCode);
            Xunit.Assert.Contains("UserName Exist", badRequestResult.Value.ToString());

        }

        [TestMethod]
        public void Register_NewUser_ReturnsCreated()
        {
            var newU = new UserModel { Username = "Herlan5", Password = "HerlanForever5", Role = "Admin" };
            var authServM = new Mock<IAuthenticServ>();
            authServM.Setup(service => service.Register(newU));
            var control = new AuthenticController(authServM.Object);
            var result = control.Register(newU);
            Xunit.Assert.IsType<StatusCodeResult>(result);
            var status = (StatusCodeResult)result;
            Xunit.Assert.Equal(201, status.StatusCode);
        }
    }
}
