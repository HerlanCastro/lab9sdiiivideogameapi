﻿using Xunit;
using Moq;
using Microsoft.AspNetCore.Mvc;
using LabVideoGameApi.Controllers;
using LabVideoGameApi.model;
using LabVideoGameApi.service;

[Collection("IntegrationTests")]
public class GameControllerIntegrationTests 
{
    [Fact]
    public void Create_TwoGames_Returns200Ok()
    {
        // Arrange
        var mockService = new Mock<IGamesServ>();
        var games = new List<GameModel>
        {
            new GameModel { Name = "Game 1", Platform = "Xbox", Price = 59M },
            new GameModel { Name = "Game 2", Platform = "Play Station", Price = 49M }
        };

        mockService.Setup(service => service.GetAllGame())
            .Returns(games.ToDictionary(game => game.Name, game => (IEnumerable<GameModel>)new List<GameModel> { game }));

        var controller = new GameController(mockService.Object);

        // Act
        var result = controller.GetAllGames();

        // Assert
        var okResult = Xunit.Assert.IsType<OkObjectResult>(result);
        Xunit.Assert.Equal(200, okResult.StatusCode);

        var returnValue = Xunit.Assert.IsType<Dictionary<string, IEnumerable<GameModel>>>(((OkObjectResult)result).Value.GetType().GetProperty("games").GetValue(((OkObjectResult)result).Value, null));

        Xunit.Assert.True(returnValue.ContainsKey("Game 1"));
        Xunit.Assert.True(returnValue.ContainsKey("Game 2"));
    }

    
    [Fact]
    public void Test_Create_VideoGames()
    {
        var game1 = new GameModel { Name = "Game 1", Platform = "Xbox", Price = 55m };
        var game2 = new GameModel { Name = "Game 2", Platform = "PlayStation", Price = 555m };

        var mockService = new Mock<IGamesServ>();
        mockService.Setup(service => service.CreateGame(It.IsAny<GameModel>()))
                        .Returns((GameModel gameDto) =>
                        {
                            return new GameModel
                            {
                                Id = Guid.NewGuid(),
                                Name = gameDto.Name,
                                Platform = gameDto.Platform,
                                Price = gameDto.Price
                            };
                        });

        var controllerTest = new GameController(mockService.Object);

        var r1 = controllerTest.Create(game1);
        var r2 = controllerTest.Create(game2);

        Xunit.Assert.IsType<StatusCodeResult>(r1);
        Xunit.Assert.IsType<StatusCodeResult>(r2);

        var statusR1 = (StatusCodeResult)r1;
        var statusR2 = (StatusCodeResult)r2;

        Xunit.Assert.Equal(201, statusR1.StatusCode);
        Xunit.Assert.Equal(201, statusR2.StatusCode);

        mockService.Verify(service => service.CreateGame(It.IsAny<GameModel>()), Times.Exactly(2));

        mockService.Verify(service => service.CreateGame(It.Is<GameModel>(game => game.Name == game1.Name)), Times.Once);
        mockService.Verify(service => service.CreateGame(It.Is<GameModel>(game => game.Name == game2.Name)), Times.Once);
    }
    
}