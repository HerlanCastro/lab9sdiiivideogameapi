﻿using Xunit;
using Moq;
using LabVideoGameApi.model;
using LabVideoGameApi.service;
using LabVideoGameApi.Controllers;
using Microsoft.AspNetCore.Mvc;
using LabVideoGameApi.DB;
using LabVideoGameApi.repositorty;
using Microsoft.EntityFrameworkCore;
using LabVideoGameApi.mapper;

namespace hhhhh
{
    public class IntegrationTests
    {
        [Fact]
        public void TestUserCanCreateAndInteractWithGame()
        {
            // Arrange
            // Mock the UserService and GameService
            var userRepoMock = new Moq.Mock<IUserRep>();
            var gameRepoMock = new Moq.Mock<IGameRep>();

            var userService = new UserService(userRepoMock.Object);
            var gameService = new GameService(gameRepoMock.Object);

            var userController = new UserController(userService);
            var gameController = new GameController(gameService);

            // Create a new user
            var newUser = new UserModel
            {
                Username = "TestUser",
                Password = "Password123",
                Role = "User"
            };

            var userList = new List<UserModel> { newUser };

            // Setup userRepoMock to return the user
            userRepoMock.Setup(x => x.GetUsers()).Returns(userList);
            userRepoMock.Setup(x => x.GetUserByUsername("TestUser")).Returns(newUser);

            // Create a new game
            var newGame = new GameModel
            {
                Name = "Fifita",
                Platform = "Xbox",
                Price = 20.99m
            };

            var gameList = new List<GameModel> { newGame };

            // Setup gameRepoMock to return the game
            gameRepoMock.Setup(x => x.GetAllGames()).Returns(gameList);
            gameRepoMock.Setup(x => x.AddGame(It.IsAny<GameModel>())).Returns(newGame);

            // Act
            // Verify that we can get the user list
            var userResult = userController.GetAllUsers();
            Xunit.Assert.NotNull(userResult);

            // Verify that we can create a new game
            var createGameResult = gameController.Create(newGame);
            Xunit.Assert.NotNull(createGameResult);

            // Verify that we can get the game list
            var gameResult = gameController.GetAllGames();
            Xunit.Assert.NotNull(gameResult);
        }

        [Fact]
        public void TestRegisterUser()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<GameDB>()
                .UseInMemoryDatabase(databaseName: "GameDB_RegisterUser")
                .Options;

            using (var context = new GameDB(options))
            {
                var userRepository = new UserRepository(context, new AutoMapper.Mapper(new AutoMapper.MapperConfiguration(cfg => cfg.AddProfile(new UserMap()))));
                var jwtUtil = new JwtU("TestIssuer", "TestSecretKey", "role");
                var authenticService = new AuthenticService(userRepository, jwtUtil);
                var controller = new AuthenticController(authenticService);

                var newUser = new UserModel
                {
                    Username = "TestUser",
                    Password = "Password123",
                    Role = "User"
                };

                // Act
                var result = controller.Register(newUser);

                // Assert
                Xunit.Assert.NotNull(result);
                Xunit.Assert.IsType<StatusCodeResult>(result);
                Xunit.Assert.Equal(201, ((StatusCodeResult)result).StatusCode);

                var registeredUser = context.Users.FirstOrDefault(u => u.Username == "TestUser");
                Xunit.Assert.NotNull(registeredUser);
                Xunit.Assert.True(BCrypt.Net.BCrypt.Verify("Password123", registeredUser.Password));
            }
        }

    }
}
